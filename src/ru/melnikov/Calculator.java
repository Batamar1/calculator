package ru.melnikov;

/**
 *Класс калькулятор
 * Содержит методы сложения, вычитания, умножения, деления, возведения в степень
 * @author Мельников Артем
 */
public class Calculator{
    /**
     * Метод сложения
     * @param x
     * @param y
     * @return возвращает результат сложения
     */
    public static double add(double x, double y){
        return x+y;
    }

    /**
     * Метод вычитания
     * @param x
     * @param y
     * @return возвращает результат вычитания
     */
    public static double sub(double x, double y){
        return x-y;
    }

    /**
     * Метод умножения
     * @param x
     * @param y
     * @return возвращает результат умножения
     */
    public static double multi(double x, double y){
        return x*y;
    }

    /**
     * Метод вещественного деления
     * @param x
     * @param y
     * @return возвращает результат деления чисел
     * @throws ArithmeticException При делении на ноль выбросит исключение
     */
    public static double div(double x, double y) throws ArithmeticException{
        if( y == 0){
            throw new ArithmeticException("Делить на ноль нельзя");
        }

        return x/y;
    }

    /**
     * Метод целочисленного деления
     * @param x
     * @param y
     * @return возвращает результат деления чисел
     * @throws ArithmeticException При делении на ноль выбросит исключение
     */
    public static int div(int x, int y) throws ArithmeticException{
        if( y == 0){
            throw new ArithmeticException("Делить на ноль нельзя");
        }

        return x/y;
    }

    /**
     * Метод возведения в степень
     * @param x
     * @param y
     * @return возвращает возведененное в степень число
     */
    public static double expon(double x, double y){
        if(y == 1){
            return x;
        }

        if(y == 0){
            return 1;
        }

        if(y == -1){
            return 1/x;
        }

        double a = x;

        if(y < 0){
            for(int i = -1; i > y; y--){
                a *=x;
            }
            a = 1/a;
        }else {
            for (int i = 1; i < y; i++) {
                a *= x;
            }
        }

        return a;
    }

    /**
     * Метод выделения остатка
     * @param x
     * @param y
     * @return возвращает остаток
     * @throws ArithmeticException При делении на ноль выбросит исключение
     */
    public static double mod(double x, double y)throws ArithmeticException{
        if(y == 0){
            throw new ArithmeticException("Делить на ноль нельзя");
        }
        return x%y;
    }
}
