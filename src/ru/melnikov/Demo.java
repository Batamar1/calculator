package ru.melnikov;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import static java.lang.System.exit;

public class Demo {
    static Scanner scanner = new Scanner(System.in);
    static final String FILES_CATALOG = "Files\\";
    static final String INPUT_FILE = "input.txt";
    static final String OUTPUT_FILE = "output.txt";
    static final String REG = "^(-?\\d+|-?\\d+.\\d+)\\s{1}[+-/*^%]\\s{1}(-?\\d+|-?\\d+.\\d+)$";

    public static void main(String[] args) {
        String data[] = input();
        String answer = getStrOutput(data);
        System.out.println(answer);

        String dataFile[] = inputFile(FILES_CATALOG + INPUT_FILE);
        answer = getStrOutput(dataFile);
        outputFile(FILES_CATALOG + OUTPUT_FILE, answer);
    }

    public static String[] input() {
        String dataS;
        do {
            System.out.println("Введите выражение в формате 'число знак число':");
            dataS = scanner.nextLine();
            if(!dataS.matches(REG)){
                System.out.println("Веденное выражение не соответствует формату.");
            }else{
                break;
            }
        } while (true);
        String data[] = dataS.split(" ");

        return data;
    }

    public static String[] inputFile(String file){
        String dataS = "";
        try{
            Scanner scanner = new Scanner(new File(file));
            dataS = scanner.nextLine();
            if(!dataS.matches(REG)) {
                System.out.println("Выражение в файле не соответствует формату(число знак число)");
                exit(1);
            }

        }catch (FileNotFoundException e){
            e.printStackTrace();
        }

        String data[] = dataS.split(" ");

        return data;
    }

    public static String getStrOutput(String[] data){
        double x = Double.valueOf(data[0]);
        double y = Double.valueOf(data[2]);
        switch (data[1]) {
            case "+":
                return (x + " + " + y + " = " + Calculator.add(x, y));
            case "-":
                return (x + " - " + y + " = " + Calculator.sub(x, y));
            case "/":
                try {
                    return (x + " / " + y + " = " + Calculator.div(x, y));
                }catch (ArithmeticException e){
                    e.printStackTrace();
                    exit(1);
                }
            case "*":
                return (x + " * " + y + " = " + Calculator.multi(x, y));
            case "%":
                try {
                    return (x + " % " + y + " = " + Calculator.mod(x, y));
                }catch (Exception e){
                    e.printStackTrace();
                    exit(1);
                }
            case "^":
                return (x + " ^ " + y + " = " + Calculator.expon(x, y));
        }

        return "";
    }

    public static void outputFile(String nameFile, String output){
        File file = new File(nameFile);
        try {
            PrintWriter out = new PrintWriter(file.getAbsoluteFile());
            out.print(output);
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}